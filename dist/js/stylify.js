'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Tabs = function () {
	function Tabs() {
		_classCallCheck(this, Tabs);

		this._prefix = window.comaStyleguideConfig.prefix;
		this.init();
	}

	_createClass(Tabs, [{
		key: 'init',
		value: function init() {
			var self = this;
			var tabsWrappers = document.getElementsByClassName(self._prefix + '-tabs-wrapper');

			if (tabsWrappers.length) {
				[].forEach.call(tabsWrappers, function (tabWrapper) {
					self.initTabsInstance(tabWrapper);
				});
			}
		}
	}, {
		key: 'initTabsInstance',
		value: function initTabsInstance(tabWrapper) {
			var self = this;
			var tabs = tabWrapper.querySelectorAll('.' + this._prefix + '-tab a');
			var tabContents = tabWrapper.querySelectorAll('.' + this._prefix + '-tab-content');

			[].forEach.call(tabs, function (tab) {
				tab.addEventListener('click', function (e) {
					e.preventDefault();

					self.setTabActive(tabs, tabContents, tab);
				});
			});
		}
	}, {
		key: 'setTabActive',
		value: function setTabActive(tabs, tabContents, activeTab) {
			var tabContent = document.getElementById(activeTab.getAttribute('data-target'));

			[].forEach.call(tabs, function (tab) {
				tab.parentNode.classList.remove('active');
			});

			[].forEach.call(tabContents, function (tabContent) {
				tabContent.classList.remove('active');
			});

			activeTab.parentNode.classList.add('active');

			if (tabContent) {
				tabContent.classList.add('active');
			}
		}
	}]);

	return Tabs;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Navigation = function () {
	function Navigation() {
		_classCallCheck(this, Navigation);

		this._prefix = window.comaStyleguideConfig.prefix;
		this.init();
	}

	_createClass(Navigation, [{
		key: 'init',
		value: function init() {
			var self = this,
			    mobileNav = document.getElementById(this._prefix + '-mobile-navigation');

			var toggleBtns = document.getElementsByClassName('js-' + this._prefix + '-toggle-mobile-nav');
			[].forEach.call(toggleBtns, function (btn) {
				btn.addEventListener('click', function (e) {
					e.preventDefault();

					mobileNav.classList.toggle(self._prefix + '-open');
					mobileNav.classList.toggle(self._prefix + '-in');
				});
			});
		}
	}]);

	return Navigation;
}();
'use strict';

window.comaStyleguideConfig = window.comaStyleguideConfig || {};
window.comaStyleguideConfig.prefix = 'stylify';

new Tabs();
new Navigation();