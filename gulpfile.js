let gulp = require('gulp');
let nunjucks = require('gulp-nunjucks');
let nunjucksRenderer = require('gulp-nunjucks-render');
let data = require('gulp-data');
let sass = require('gulp-sass');
let concat = require('gulp-concat');
let cssnano = require('gulp-cssnano');
let rename = require('gulp-rename');
let del = require('del');
let babel = require('gulp-babel');
let notify = require('gulp-notify');
let uglify = require('gulp-uglify'); 
let header = require('gulp-header');


/**
 * Set nunjucks root folder to /src
 */  
nunjucksRenderer.nunjucks.configure(['./src']);

/**
 * Error handling
 */  
function handleError(err) {
    console.log('ERROR HANDLER: ', err.toString());
    this.emit('end');
}

/**
 * Add globals to nunjucks environment
 */  
function manageEnvironment(environment) {
	// Return the context of the current view, OR return a function of the current context
	environment.addGlobal('getContext' , function(name, ctx) {
		ctx = ctx ? ctx : this.ctx;
		return (name) ? ctx[name] : ctx;
	})
	// Run a macro by a given name and pass on some arguments
	environment.addGlobal('runMacro' , function(fn, args) {
		if (typeof fn == 'function' && args) {
			return fn(...args);
		} else if (typeof fn == 'function') {
			return fn();
		} else {
			console.log('fn is not a function' , typeof fn);
		}
	})
}

/**
 * Require the config.json from the brand folder and add the current timestamp
 */  
function getData() {
	let data = {};

	return data;
}

/**
 * Format the current timestamp like dd.mm.yyyy
 */  
function formatTimestamp() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	var today = dd+'.'+mm+'.'+yyyy;
	return today;
}

/**
 * Compile and concatenate the styleguide Stylesheets.
 */  
gulp.task('sass', function () {
	return gulp.src('./src/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./dist/css'));
});

/**
 * Concatenate and minify the styleguide Javascripts.
 */ 
gulp.task('scripts', function () {
    return gulp.src(['./src/js/tabs.js', './src/js/navigation.js', './src/js/stylify.js'])
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', handleError)
        .pipe(concat('stylify.js'))
        .on('error', handleError)
        .pipe(gulp.dest('./dist/js'))
        .on('error', handleError)
        .pipe(rename('stylify.min.js'))
        .on('error', handleError)
        .pipe(uglify())
        .on('error', handleError)
        .pipe(gulp.dest('./dist/js'))
        .on('error', handleError)
        .pipe(notify({message: 'Styleguide Scripts task complete'}))
        .on('error', handleError);
});

/**
 * Default task, do everyting.
 */  
gulp.task('default', ['sass', 'scripts']);




