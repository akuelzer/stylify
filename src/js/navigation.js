class Navigation {

	constructor() {
		this._prefix = window.comaStyleguideConfig.prefix;
		this.init();
	}

	init() {
		let self = this,
			mobileNav = document.getElementById(this._prefix + '-mobile-navigation');
		
		let toggleBtns = document.getElementsByClassName('js-' + this._prefix + '-toggle-mobile-nav');
		[].forEach.call(toggleBtns, function(btn) {
			btn.addEventListener('click', function(e) {
				e.preventDefault();

				mobileNav.classList.toggle(self._prefix + '-open');
				mobileNav.classList.toggle(self._prefix + '-in');
			});
		});

	}
}
