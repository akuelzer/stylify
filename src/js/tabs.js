class Tabs {

	constructor() {
		this._prefix = window.comaStyleguideConfig.prefix;
		this.init();
	}

	init() {
		let self = this;
		let tabsWrappers = document.getElementsByClassName(self._prefix + '-tabs-wrapper');

		if (tabsWrappers.length) {
			[].forEach.call(tabsWrappers, function(tabWrapper) {
			  self.initTabsInstance(tabWrapper);
			});
		}
	}

	initTabsInstance(tabWrapper) {
		let self = this;
		let tabs = tabWrapper.querySelectorAll('.' + this._prefix + '-tab a');
		let tabContents = tabWrapper.querySelectorAll('.' + this._prefix + '-tab-content');

		[].forEach.call(tabs, function(tab) {
			tab.addEventListener('click', function(e) {
				e.preventDefault();

				self.setTabActive(tabs, tabContents, tab);
			});
		});
	}

	setTabActive(tabs, tabContents, activeTab) {
		let tabContent = document.getElementById(activeTab.getAttribute('data-target'));
		
		[].forEach.call(tabs, function(tab) {
			tab.parentNode.classList.remove('active');
		});

		[].forEach.call(tabContents, function(tabContent) {
			tabContent.classList.remove('active');
		});

		activeTab.parentNode.classList.add('active');

		if (tabContent) {
			tabContent.classList.add('active');
		}
	}
}
